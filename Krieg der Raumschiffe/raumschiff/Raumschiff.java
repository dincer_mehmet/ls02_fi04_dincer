package raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemenInProzent;
	private int andoidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff(String string, int i, int j, int k, int l, int m, int n, ArrayList<String> arrayList) {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemenInProzent = 0;
		this.andoidenAnzahl = 0;
		this.schiffsname = "Schiff";
		
	}

	public Raumschiff(String string, int energieversorgungInProzent, int zustandSchildenInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlnDroiden,
			ArrayList<String> arrayList) {

	}

	public Raumschiff() {
		
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemenInProzent() {
		return lebenserhaltungssystemenInProzent;
	}

	public void setLebenserhaltungssystemenInProzent(int lebenserhaltungssystemenInProzent) {
		this.lebenserhaltungssystemenInProzent = lebenserhaltungssystemenInProzent;
	}

	public int getAndoidenAnzahl() {
		return andoidenAnzahl;
	}

	public void setAndoidenAnzahl(int andoidenAnzahl) {
		this.andoidenAnzahl = andoidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);

	}

	public void photonentorpedoSchiessen(Raumschiff r) {
		if(getPhotonentorpedoAnzahl() < 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			setPhotonentorpedoAnzahl(photonentorpedoAnzahl -1);
			treffer(r);
		}
		
	}
		
		public void phaserkanoneschiessen(Raumschiff r) {
			if(getEnergieversorgungInProzent() < 50) {
				nachrichtAnAlle("-=*Click*=-");
			}
			else {
				setEnergieversorgungInProzent(energieversorgungInProzent -50);
				treffer(r);
				nachrichtAnAlle("Phaserkanone abgeschossen!");
			}
	}

	private void treffer(Raumschiff r) {
		
		schildeInProzent = schildeInProzent - 50;
		if(schildeInProzent  < 1) {
			huelleInProzent = huelleInProzent -50;
			energieversorgungInProzent = energieversorgungInProzent -50;
			if(huelleInProzent < 1) {
				lebenserhaltungssystemenInProzent = 0;
				System.out.println("Lebenserhaltungssyteme von" + r.getSchiffsname() + "wurde getroffen!");
			}
		
		}
		System.out.printf(this.getSchiffsname() + "wurde getroffen!");

	}
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;

	}

	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffschuelle,
			int anzahlDroiden) {
	}

	public void zustandRaumschiff() {
		broadcastKommunikator.add(Integer.toString(getSchildeInProzent()));
		broadcastKommunikator.add(Integer.toString(getHuelleInProzent()));
		broadcastKommunikator.add(Integer.toString(getLebenserhaltungssystemenInProzent()));
		broadcastKommunikator.add(Integer.toString(getEnergieversorgungInProzent()));
		
		System.out.println(broadcastKommunikator);
	}
		public void ladungsverzeichnisAusgaben() {
			if(this.ladungsverzeichnis.size() <=0) {
				System.out.println("Es ist keine Ladung im  Ladungsverzeichniss vorhanden");
			}
			else {
				for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
				System.out.println(this.ladungsverzeichnis.get(i).getBezeichnung()+":"+this.ladungsverzeichnis.get(i).getMenge());
				
				}
		}
	}

	

	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			if(this.ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}

	}
	
	public void phaserkanonenschiessen(Raumschiff r) {
		if(this.energieversorgungInProzent > 50 ) {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			this.nachrichtAnAlle(schiffsname);
			//treffer aufrufen
		}
		else {
			//nachricht an alle --Click--
			
		}
	}

	public void LadungEingehend(Ladung feregniS) {
		
	}
}
