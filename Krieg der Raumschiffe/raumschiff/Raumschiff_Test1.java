package raumschiff;

import java.util.ArrayList;

public class Raumschiff_Test1 {

	private static Raumschiff vulkanier;
	private static Raumschiff romulaner;
	private static Raumschiff klingonen;

	public static void main(String[] args) {
	
		setKlingonen(new Raumschiff("IKS Hegh�ta", 100, 100, 100, 100, 1, 2, new ArrayList<String>()));
		setRomulaner(new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, new ArrayList<String>()));
		setVulkanier(new Raumschiff("Ni�var", 80, 80, 100, 50, 0, 5, new ArrayList<String>()));
		
		Ladung feregniS = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgS = new Ladung("Borg-Schrott", 5);
		Ladung roteM =new Ladung("Rote Materie", 2);
		Ladung forschungsS = new Ladung("Forschungssonde", 35);
		Ladung batKS = new Ladung("Bat�leth Klingonen Schwert", 200);
		Ladung plasmaW = new Ladung("Plasma-Waffe", 50);
		Ladung photonenT = new Ladung("Photonentorpedo", 3);
		
		klingonen.LadungEingehend(feregniS);
		klingonen.LadungEingehend(batKS);
		romulaner.LadungEingehend(borgS);
		romulaner.LadungEingehend(roteM);
		romulaner.LadungEingehend(plasmaW);
		vulkanier.LadungEingehend(forschungsS);
		vulkanier.LadungEingehend(photonenT);
		
	}

	public static Raumschiff getVulkanier() {
		return vulkanier;
	}

	public static void setVulkanier(Raumschiff vulkanier) {
		Raumschiff_Test1.vulkanier = vulkanier;
	}

	public static Raumschiff getRomulaner() {
		return romulaner;
	}

	public static void setRomulaner(Raumschiff romulaner) {
		Raumschiff_Test1.romulaner = romulaner;
	}

	public static Raumschiff getKlingonen() {
		return klingonen;
	}

	public static void setKlingonen(Raumschiff klingonen) {
		Raumschiff_Test1.klingonen = klingonen;
	}

}
