
public class Rechteck {

	private double SeiteA, SeiteB;

	public Rechteck() {
	}

	public Rechteck(double seiteA, double seiteB) {
		this.SeiteA = seiteA;
		this.SeiteB = seiteB;

	}

	public double getSeiteA() {
		return SeiteA;
	}

	public void setSeiteA(double seiteA) {
		this.SeiteA = seiteA;
	}

	public double getSeiteB() {
		return SeiteB;
	}

	public void setSeiteB(double seiteB) {
		this.SeiteB = seiteB;
	}

	public double getFl�che() {
		return SeiteA * SeiteB;
	}

	public double getUmfang() {
		return SeiteA * 2 + 2 * SeiteB;
	}

}